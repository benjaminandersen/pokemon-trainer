# Pokémon Trainer
This application was created using Angular framework.


You can find the app here: https://dry-depths-90501.herokuapp.com/login

You can view the component tree here: https://gitlab.com/benjaminandersen/pokemon-trainer/-/blob/master/Component_tree.pdf

## Description
It is an application for browsing and catching Pokémon, and consists of three pages: 
- Landing page
- Pokémon catalogue page
- Trainer page


## Functionality
### Landing page
The landing page takes input from the user and logs them in if the user exists in the database. If the user doesn't exist, it will register a new user to the database and then log them in. When login is successful, the user is sent to the Pokémon catalogue page.

### Pokémon catalogue page
The Pokémon catalogue page displays Pokémon. The Pokémon from the Pokémon API are stored in local storage. To catch a Pokémon, the user can click on a Pokémon.

### Trainer page 
The trainer page displays the logged in user's collected Pokémon. Users can let Pokémon go by clicking a button.
The page has one button, to navigate back to the Pokédex.

Disclaimer: Due to the API properties of already existing trainer Ash, the first two Pokémon will not show correctly. However, if you add Pokémon to this user, the rest will render correctly.

#### Navbar. 
The navbar has a title and two buttons: profile and logout. By clicking the profile buton, the user is taken to the trainer page. By clicking the logout button, the user is logged out, and redirected to the login page.

## API
Two APIs were used for data storage and collection.

The program uses a JSON server to keep track of users and their Pokémon. You can see it here: https://noroff-assignment-api-emoye.herokuapp.com/trainers

For data collection, this API was used: https://pokeapi.co

## Contributors
 * Embla Øye
 * Benjamin Andersen
 * Sunniva Stolt-Nielsen
