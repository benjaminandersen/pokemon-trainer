export interface Pokemon {
    id: string | undefined;
    name: string;
    sprite: string;
    url: string;
}

export interface PokemonResponse {
    count: number;
    next: string;
    previous: string;
    results: Pokemon[];
}

