import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Pokemon, PokemonResponse } from "../models/pokemon.model";
import { SessionService } from "./session.service";

const baseUrl = environment.pokeBaseUrl;
const baseSpriteUrl = environment.spriteBaseUrl;

@Injectable({
    providedIn: 'root'
})

export class PokemonService {
    private _pokemons: Pokemon[] = [];
    private _error: string = '';

    constructor(
        private readonly http: HttpClient,
        private sessionService: SessionService
        ) { }

    

    /**
     * fetches pokemons from baseUrl API
     */
    public fetchPokemon(): void {
        // Observables
        this.http.get<PokemonResponse>(baseUrl)
            .subscribe((response: PokemonResponse) => {
                this._pokemons = response.results;
                this.getIdAndSprite();
                this.sessionService.setPokemon(this._pokemons)
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            }
        )
    }

    /**
     * fetches pokemon id and sprite based on id of pokemon fetched in fetchPokemon
     */
    public getIdAndSprite = (): void => {
        this._pokemons.map(pokemon => {
            pokemon.id = pokemon.url.split('/').filter(Boolean).pop();
            pokemon.sprite = `${baseSpriteUrl}${pokemon.id}.png` 
        })    
    }

    public pokemons(): Pokemon[] {
        return this._pokemons;
    }

    public error(): string {
        return this._error;
    }
}