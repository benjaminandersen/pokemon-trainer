import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { SessionService } from "./session.service";
import { catchError, finalize, map, retry, switchMap, tap } from "rxjs/operators";
import { User } from "../models/user.model";
import { Pokemon } from "../models/pokemon.model";

const userApi = environment.noroffBaseUrl;

@Injectable({
    providedIn: 'root'
})

export class UserService {

    public attempting: boolean = false;
    public error: string = '';

    constructor(
        private readonly http: HttpClient,
        private sessionService: SessionService
        ){            
        }
    
    /**
     * gets user from API
     */
    private findByUsername(username: string): Observable<User[]> {
        return this.http.get<User[]>(`${ userApi }?username=${ username }`)
    }

    /**
     * creates a new user in the API with username, id and array of pokemon
     */
    private createUser(username: string): Observable<User> {
        const headers = new HttpHeaders({
            'x-api-key': environment.apiKey
        })
        return this.http.post<User>(`${ userApi }`, { username, pokemon: []}, { headers })
    }


    /**
     * checks if user exists in the API or not, creates new user if not
     * logs in user and sets user in local storage
     */
    public authenticate(username: string, onSuccess: () => void): void {
        
        this.attempting = true;

        /**
         * if a user is not found, switch to Register
         */
        const mapToRegister = (users: User[]) => {
            if (users.length) {
                return of(users[0])
            }
            return this.createUser(username)
        }

        /**
         * catch general errors
         */
        const catchRequestError = (user: User) => {
            return throwError('Could not create a user.');
        }

        /**
         * put user in session
         */
        const tapToSession = (user: User) => {
            this.sessionService.setUser(user)
        }

        /**
         * finalize response
         */
        const finalizeRequset = () => this.attempting = false;

        this.findByUsername(username)
            .pipe(
                retry(3),
                switchMap(mapToRegister),
                tap(tapToSession),
                catchError(catchRequestError),
                finalize(finalizeRequset)
            )
            .subscribe(
                (user: User) => { // On success
                    if (user.id) {
                        this.sessionService.setUser(user)
                        onSuccess();
                    }
                    
                },
                (error: string) => { // On error
                    this.error = error;
                }
            )
    }

    /**
     * update user's collected pokemon in the API
     * first gets the pokemon the current user has collected from local storage
     * then adds collectedPokemon to array, and updates the pokemon list in the API
     * updates the session with the updated pokemon list
     * @param userId - logged in user's id
     * @param username - logged in user's username
     * @param collectedPokemon - pokemon user clicks on to collect
     */
    public updatePokemonList(userId: number | undefined, username: string, collectedPokemon: Pokemon ) {
        let updatedPokemon: Pokemon [] = JSON.parse(localStorage.getItem("user") || "{}").pokemon

        /**
         * add collectedPokemon to updatedPokemon
         */
        updatedPokemon?.push(collectedPokemon)
        
        const requestOptions = {
            headers: {
                'X-API-Key': environment.apiKey,
                'Content-Type': 'application/json'
            },
        };
        this.http.patch<User>(`${ userApi }/${ userId }`,{ pokemon: updatedPokemon }, requestOptions )
        .subscribe(
            (updatedUser: User) => {
                this.authenticate(username, () => {})
            },
            (error: HttpErrorResponse) => {
                this.error = error.message;
            }
        );
    }

    /**
     * remove a pokemon fra user's collected pokemon in the API
     * first gets the pokemon the current user has collected from local storage
     * then removes pokemon from the pokemon list by filtering out pokemons with same name as collectedPokemon
     * updates the session with the updated pokemon list
     * @param userId - logged in user's id
     * @param username - logged in user's username
     * @param collectedPokemon - pokemon user clicks on to collect
     */
    public removePokemon(userId: number | undefined, username: string, collectedPokemon: Pokemon ) {
        let updatedPokemon: Pokemon [] = JSON.parse(localStorage.getItem("user") || "{}").pokemon

        /**
         * remove the pokemon with same name as collectedPokemon from updatedPokemon
         */
        updatedPokemon = updatedPokemon.filter(p => p.name !== collectedPokemon.name)
        
        const requestOptions = {
            headers: {
                'X-API-Key': environment.apiKey,
                'Content-Type': 'application/json'
            },
        };
        this.http.patch<User>(`${ userApi }/${ userId }`,{ pokemon: updatedPokemon }, requestOptions )
        .subscribe(
            (updatedUser: User) => {
                this.authenticate(username, () => {})
            },
            (error: HttpErrorResponse) => {
                this.error = error.message;
            }
        );
    }
}
