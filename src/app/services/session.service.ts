import { Injectable } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { User } from "../models/user.model";

/**
 * gets and sets different data to local storage and session storage accordingly
 */
@Injectable({
    providedIn: 'root'
})

export class SessionService {
    private _pokemons: Pokemon[] = [];
    private _user: User | undefined;

    constructor() {
        const storedPokemon = sessionStorage.getItem("pokemon" || "{}");
        if (storedPokemon) {
            this._pokemons = JSON.parse(storedPokemon) as Pokemon[];
        }

        const storedUser = localStorage.getItem('user' || "{}");
        if (storedUser) {
            this._user = JSON.parse(storedUser) as User;
        }
    }

    get user(): User | undefined {
        return this._user;
    }

    setUser(user: User): void {
        this._user = user;
        localStorage.setItem('user' || "{}", JSON.stringify(user));
    }

    /**
     * check if a user is logged in and set true/false accordingly
     */
    isLoggedIn() {
        return this._user !== undefined;
    }

    logout() {
        this._user = undefined;
        localStorage.removeItem('user' || "{}");
    }

    get pokemons(){
        return this._pokemons;
    }

    setPokemon(pokemon: Pokemon[]): void {
        this._pokemons = pokemon;
        sessionStorage.setItem('pokemon' || "{}", JSON.stringify(pokemon));
    }

    getCollectedPokemon() {
        return JSON.parse(localStorage.getItem('user') || '{}').pokemon;

    }
}