import { Component } from '@angular/core'
import { SessionService } from 'src/app/services/session.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer-page.component.html',
    styleUrls: ['./trainer-page.component.css']
}) // Decorators
export class TrainerPage {

    constructor(
        private readonly sessionService: SessionService,
        private readonly userService: UserService,
        private readonly router: Router
        ) {}

    /**
     * get a user's collected pokemon from local storage
     */
    get collectedPokemons(): Pokemon[] {
        return this.sessionService.getCollectedPokemon();
    }

    /**
     * navigate to pokemon-catalogue page when user clicks "Back to Pokedex"
     */
    onPokedexClick() {
        this.router.navigate(["pokedex"])
    }

    /**
     * delete all pokemon with name of clicked pokemon from user's collected pokemon in the API
     */
    onDeleteClick(pokemon: Pokemon) {
        this.userService.removePokemon(this.sessionService.user?.id, JSON.parse(localStorage.getItem("user") || "{}").username, pokemon)
    }

    /**
     * get logged in user's username
     */
     username = this.sessionService.user?.username;
}
