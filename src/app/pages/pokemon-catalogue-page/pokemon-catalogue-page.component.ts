import { Component, OnInit } from '@angular/core'
import { PokemonService } from '../../services/pokemons.service';
import { Pokemon } from '../../models/pokemon.model';
import { SessionService } from '../../services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-pokemon-catalogue-page',
    templateUrl: './pokemon-catalogue-page.component.html',
    styleUrls: ['./pokemon-catalogue-page.component.css']
}) // Decorators
export class PokemonCataloguePage implements OnInit{

    constructor(
        private readonly pokemonService: PokemonService,
        private readonly sessionService: SessionService,
        private readonly userService: UserService
        ) 
        {
    }

    /**
     * fetches pokemon from pokemon API if there aren't pokemon in session storage
     */
    ngOnInit(): void {
        if (sessionStorage.getItem("pokemon") === null) {
            this.pokemonService.fetchPokemon();
        }
    }

    get pokemons(): Pokemon[] {
        return this.sessionService.pokemons;
    }

    onCollectClick(pokemon: Pokemon) {
        console.log(pokemon);
        this.userService.updatePokemonList(this.sessionService.user?.id, JSON.parse(localStorage.getItem("user") || "{}").username, pokemon)
    }

    /**
     * checks if a pokemon has been caught by the user
     * @param pokemon 
     * @returns 
     */
    checkIfCaught(pokemon: Pokemon) {
        return this.sessionService.user?.pokemon.find(p => p.name === pokemon.name)
    }
}