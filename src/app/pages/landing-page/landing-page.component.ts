import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { SessionService } from "src/app/services/session.service";
import { UserService } from "src/app/services/user.service";


@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.css']
})
export class LandingPage implements OnInit {

    constructor(
        private readonly router: Router,
        private readonly userService: UserService,
        private readonly sessionService: SessionService
    ){
    }

    /**
     * if there is a user in local storage, redirect user to pokemon catalogue
     */
    ngOnInit() {
        if (this.sessionService.user !== undefined) {
            this.router.navigate(['pokedex'])
        }
    }

    get attempting(): boolean {
        return this.userService.attempting;
    }

    /**
     * logins in user and reroutes to pokedex on success
     */
    onSubmit(loginForm: NgForm): void {
        const { username } = loginForm.value
        this.userService.authenticate(username, async () => {
            await this.router.navigate(['pokedex'])
        })
    }
}