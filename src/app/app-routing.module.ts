import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingPage } from "./pages/landing-page/landing-page.component";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue-page/pokemon-catalogue-page.component";
import { TrainerPage } from "./pages/trainer-page/trainer-page.component";
import { AuthGuard } from "./services/auth.guard";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LandingPage
    },
    {
        path: 'pokedex',
        component: PokemonCataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'pokemon-caught',
        component: TrainerPage,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}