import { Component, OnChanges } from "@angular/core";
import { SessionService } from "src/app/services/session.service";
import { Router } from "@angular/router";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {
    constructor(
        private readonly sessionService: SessionService,
        private readonly router: Router,
    ){
        // when router changes, check if user is logged in or not and set loggedIn and username accordingly
        router.events.subscribe( () => {
            this.loggedIn = this.sessionService.isLoggedIn();
            this.username = this.sessionService.user?.username;
        });
    }

    /**
     * get logged in user's username
     */
    username = this.sessionService.user?.username;

    /**
     * check if user is logged in or not, used to toggle logout button
     */
    loggedIn = this.sessionService.isLoggedIn();

    /**
     * logout user and navigate to landing page when user clicks on "Logout" button in navbar
     */
    onLogoutClick() {
        this.sessionService.logout();
        this.router.navigate(["login"]);
    }

    /**
     * navigate to pokemon-catalogue page when user clicks on "Trainer" button in navbar
     */
    onTrainerClick() {
        this.router.navigate(["pokemon-caught"])
    }
}