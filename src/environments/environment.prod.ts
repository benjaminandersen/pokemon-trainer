/**
 * get APIs and API key for application
 */
export const environment = {
  production: true,
  pokeBaseUrl: "https://pokeapi.co/api/v2/pokemon?limit=151",
  spriteBaseUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",
  noroffBaseUrl: "https://noroff-assignment-api-emoye.herokuapp.com/trainers",
  apiKey: "9gBLjmloJSGc4aXYAgCecUmt4RZL1GOGy40wEQwCtQZzMXlgk8ZIWnaBNrOSvwCn"
};
